package adpro.article.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import adpro.article.core.Writer;
import adpro.article.repository.ArticleRepository;

@Service
public class ArticleServiceImpl implements ArticleService {
    @Autowired
    private ArticleRepository articleRepository;

    public void addWriter(Writer writer) {
        if (checkWriterExist(writer)) return;
        Writer newWriter = articleRepository.save(writer);
    }

    public boolean checkWriterExist(Writer writer) {
        List<Writer> existing = articleRepository.findByTitleAndFullname(writer.getTitle(), writer.getFullname());
        if (existing.isEmpty()) return false;
        return true;
    }

    public List<Writer> getWriters() {
        Iterable<Writer> iterable = articleRepository.findAll();
        List<Writer> result = new ArrayList<Writer>();
        iterable.forEach(result::add);
        return result;
    }

    public Writer getWriter(Long id) {
        return articleRepository.findById(id).get();
    }
}