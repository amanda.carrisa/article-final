package adpro.article.repository;

import org.springframework.data.repository.CrudRepository;
import adpro.article.core.Comment;

public interface CommentRepository extends CrudRepository<Comment, Long>{

}
