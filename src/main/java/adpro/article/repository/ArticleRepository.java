package adpro.article.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import adpro.article.core.Writer;

public interface ArticleRepository extends CrudRepository<Writer, Long> {
    List<Writer> findByTitleAndFullname(String title, String fullname);
}
