package adpro.article.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import java.util.List;
import adpro.article.core.Comment;
import adpro.article.core.Writer;
import adpro.article.service.ArticleService;
import adpro.article.service.ArticleServiceImpl;
import adpro.article.service.CommentService;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
public class ArticleController {
    @Autowired
    private ArticleService service;
    @Autowired
    private CommentService commentService;
    private List<Writer> articlesList;

    @RequestMapping(value="/article", method=RequestMethod.GET)
    public String displayArticle(Model model) {
        articlesList = service.getWriters();
        model.addAttribute("list", articlesList);
        return "home";
    }

    @RequestMapping(value = "/article/add", method = RequestMethod.GET)
    public String getFormAddArticle(Model model) {
        model.addAttribute("writer", new Writer());
        return "add-article";
    }
    
    @RequestMapping(value="/article/add-writer", method=RequestMethod.POST)
    public String addArticle(@ModelAttribute("writer") Writer writer) {
        service.addWriter(writer);
        return "redirect:/article";
    }

    @RequestMapping(value="/article/detail", method=RequestMethod.GET)
    public String viewArticle(
        @RequestParam(value="idArticle") Long id,
        Model model
    ) {
        Writer article = service.getWriter(id);
        model.addAttribute("article", article);
        model.addAttribute("listcomment", article.getListComment());
        model.addAttribute("comment", new Comment());
        return "view-article";
    }

    @GetMapping("/article/detail")
    @ResponseBody
    public String addComment(@RequestParam Comment comment) {
        commentService.addComment(comment);
        return comment.getCommented();
    }
}