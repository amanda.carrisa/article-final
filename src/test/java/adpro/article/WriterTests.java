package adpro.article;

import adpro.article.core.Writer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class WriterTests {
    private Writer writer;

    @BeforeEach
    public void setUp()throws Exception {
        this.writer = new Writer();
    }

    @Test
    public void testSetWriterFullName() throws Exception {
        writer.setFullname("fullname");
        String hasil = writer.getFullname();
        Assertions.assertEquals("fullname", hasil);
    }

    @Test
    public void testSetTitle() throws Exception {
        writer.setTitle("title");
        String hasil = writer.getTitle();
        Assertions.assertEquals("title", hasil);
    }

    @Test
    public void testSetEmail() throws Exception {
        writer.setEmail("title");
        String hasil = writer.getEmail();
        Assertions.assertEquals("title", hasil);
    }

    @Test
    public void testSetBody() throws Exception {
        writer.setBody("title");
        String hasil = writer.getBody();
        Assertions.assertEquals("title", hasil);
    }


}